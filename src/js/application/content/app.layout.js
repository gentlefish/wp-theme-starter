
App.Layout = (function () {

    var $  = jQuery.noConflict();

    // VARIABLES

    // ELEMENTS

    return {

        init: function () {

            $(document).ready( function() {

                $('.main-nav__toggle').on('click', function (event) {
                    event.preventDefault();
                    $('body').toggleClass('body--menu-visible');
                });


                // $('.accordeon').on('click', function (event) {
                //     event.preventDefault();
                //     $(this).toggleClass('accordeon--open');
                // })


                // $('.main-nav .menu-item-has-children a').on('click', function (event) {
                //     console.log($('.main-nav .menu-item-has-children'));
                //     $('.main-nav .menu-item-has-children').removeClass('submenu-open');
                //     $(this).parent('.menu-item-has-children').toggleClass('submenu-open')
                // });

                //swiper
                // var swiperArticles = new Swiper('.swiper-container', {
                //     loop: true,
                //     navigation: {
                //         nextEl: '.swiper-button-next',
                //         prevEl: '.swiper-button-prev',
                //     },
                //     pagination: {
                //         el: '.swiper-pagination',
                //         type: 'fraction',
                //     },
                // });


                // // lightbox
                // var lightbox = $('.gallery a').simpleLightbox();


            });

        },

    }

})();
