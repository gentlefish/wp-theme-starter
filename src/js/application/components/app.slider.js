App.Slider = (function () {

    var $  = jQuery.noConflict();

    // VARIABLES
    // temps entre 2 slides
    var timer = 3000;  // 5000
    var animation = 1500;  
    var easing = 'easeInOutCubic';
    var slideStart = 0;
    var slideCurrent = slideStart;
    var slidePrevious = slideStart;
    var posSlider, 
        slidesNbr,
        sliderLoop;

    // ELEMENTS
    var sliderClassName = "slider" ;
    var slideClassName = "slide" ;


    return {

        init: function () {
            console.log('Slider.init');
            
            $(document).ready( function() {
                $("." + sliderClassName).each( function () {
                    if( $("." + slideClassName, $(this)).length > 1 ) {
                        App.Slider.sliderInit($(this));
                    }
                });
            });
        },


        sliderInit: function ( $slider ) {
            console.log('Slider.sliderInit');

            $slider.addClass("slider--js-actif");

            slidesNbr = $("." + slideClassName, $slider).length;

            // LOOP
            sliderLoop = setInterval( function () {
                slideCurrent++;

                if(slideCurrent >= slidesNbr) {
                    slideCurrent = 0;
                }
                
                App.Slider.sliderGoTo( $slider);
            }, timer);


            App.Slider.sliderCreateDotNav( $slider, slideCurrent );
            App.Slider.sliderCreateArrow( $slider, slidesNbr );
            // App.Slider.sliderBindSwipeNav( $slider, slidesNbr );

        },


        sliderGoTo: function ( $slider) {
            // console.log('from '+slidePrevious+' to '+slideCurrent);

            $('.'+ slideClassName +':eq(' + slideCurrent + ')', $slider)
                .css({
                    'opacity': 0,
                    'z-index': 2
                })
                .animate({
                    'opacity': 1,
                }, animation)

            $('.'+ slideClassName +':eq(' + slidePrevious + ')', $slider)
                .animate({
                    'opacity': 0,
                }, animation)
                .promise().done(function() {
                    $(this).css({
                        'z-index': 1
                    })
                });

            slidePrevious = slideCurrent;
        },


        sliderCreateDotNav: function ( $slider, slideCurrent ) {
            console.log('Slider.sliderCreateDotNav');

            // points de navigation
            $slider.append('<ul class="slider__dot-navigation" aria-hidden="true"></ul>')
                   .promise().done(function () {
                       var $dotNavigation = $('.slider__dot-navigation', $slider);

                        $('.slide', $slider).each(function() {
                            $dotNavigation.append('<li class="slider__dot"></li>');
                        }).promise().done(function () {
                            $('.slider__dot', $dotNavigation).on('click', function () {
                                // console.log("coucou");
                                clearInterval(sliderLoop);
                                slideCurrent = $(this).index();
                                App.Slider.sliderGoTo( $slider);
                            });
                        });;
                   }); 
            
        },

        sliderUpdateDotNav: function ( $slider, slideCurrent ) {

        },


        sliderCreateArrow: function ( $slider, slidesNbr ) {
            console.log('Slider.sliderCreateArrow');

            // création de la nav prev / next & bind

            $slider.append('<a href="#" class="slider__arrow slider__arrow--prev" title="Slide précédente" rel="nofollow" aria-hidden="true"></a>')
                   .promise().done( function() {
                        $slider.children('.slider__arrow--prev').on( 'click', function ( event ) {
                            event.preventDefault();
                            clearInterval(sliderLoop);
                            slideCurrent--;

                            if(slideCurrent <= 0) {
                                slideCurrent = slidesNbr - 1;
                            }

                            App.Slider.sliderGoTo( $slider);
                        });
                   });

            $slider.append('<a href="#" class="slider__arrow slider__arrow--next" title="Slide suivante" rel="nofollow" aria-hidden="true"></a>')
                   .promise().done( function() {
                        $slider.children('.slider__arrow--next').off( 'click' ).on( 'click', function ( event ) {
                            event.preventDefault();
                            clearInterval(sliderLoop);

                            slideCurrent++;

                            if(slideCurrent >= slidesNbr) {
                                slideCurrent = 0;
                            }
                            
                            App.Slider.sliderGoTo( $slider);
                        });
                   });

        },


        sliderBindSwipeNav: function ( $slider, $sliderContent, slidesNbr ) {
            console.log('Slider.sliderBindNav');

            // interaction au swipe (hammerjs)
            $slider.hammer().on("swiperight", function () {
                clearInterval(sliderLoop);
                slideCurrent--;

                if(slideCurrent <= 0) {
                    slideCurrent = slidesNbr - 1;
                }

                App.Slider.sliderGoTo( $slider);
            });

            $slider.hammer().on("swipeleft", function () {
                clearInterval(sliderLoop);
                slideCurrent++;

                if(slideCurrent >= slidesNbr) {
                    slideCurrent = 0;
                }
                
                App.Slider.sliderGoTo( $slider);
            });
        },


    };

})();
