
App.LetterDefil = (function () {

    var $  = jQuery.noConflict();

    // VARIABLES

    // ELEMENTS


    return {
        init: function () {
            ('.letter').each(function() {
                // console.log(makeid());

                var target = $(this).data('target');
                var timing = $(this).data('timing-ms');
                var letterDefil = $(this).children('.letter__defil');

                let timingLetter = setInterval(function(){
                    $(letterDefil).html(makeLetter);
                }, 55);
                setTimeout(() => {
                    clearInterval(timingLetter);
                    $(letterDefil).html(target);
                }, timing);
            });
        },

        makeLetter: function () {
          var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
          return possible.charAt(Math.floor(Math.random() * possible.length));
        }

    }
});