
App.Popin = (function () {

    var $  = jQuery.noConflict();

    // VARIABLES
    var popinOverlayClass = 'popin__overlay',
        popinCloseClass = 'popin__close',
        popinOpenClass = 'popin__open',
        popinOverlayClass = 'popin__overlay',
        popinHiddenClass = 'popin--hidden';

    // ELEMENTS

    return {

        init: function () {

            // ne focntionne qu'avec une seule popin dans la page

            $(document).ready( function() {

                $('.' + popinOpenClass).on('click', function (event) {
                    event.preventDefault();
                    $('body').removeClass(popinHiddenClass);
                    $('#s').focus();

                    // key échap désactivé
                    $(document).off('keyup');

                });

                $('.' + popinCloseClass + ', .' + popinOverlayClass).on('click', function (event) {
                    event.preventDefault();
                    $('body').addClass(popinHiddenClass);
                    $('#s').blur();
                });

                // key échap
                $(document).on('keyup', function(e) {
                    if (e.keyCode === 27) {
                        $('body').addClass(popinHiddenClass);
                        $('#s').blur();
                    }
                });

            });
        },


    }

})();
