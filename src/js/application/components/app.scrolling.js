
// scrolling
App.Scrolling = (function () {

    var $  = jQuery.noConflict();

    return {

        init: function () {
            // console.log("Scrolling.init");

            $(document).ready( function() {

                var lastScrollTop = 0;

                $(window).on('scroll', function() {
                   var st = $(this).scrollTop();
                   // si on ne voit plus le header
                    if (st > 0) {
                        // scoll > 0 => mini header
                        $('body').addClass('body--scroll')
                    } else {
                        // scoll 0 => on est haut de la page
                        $('body').removeClass('body--scroll')
                    }

                    if (st < lastScrollTop){
                        // console.log("up");
                        // show le header
                        $('body').addClass('body--scroll-up')
                    } else {
                        // console.log("down");
                        $('body').removeClass('body--scroll-up')
                    }
                    lastScrollTop = st;
                });
            });
        },
    }

})();
