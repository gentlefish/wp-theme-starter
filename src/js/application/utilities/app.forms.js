App.Forms = (function () {

    var $  = jQuery.noConflict();


    // VARIABLES
    var select2Activated = false;
    var hasSelect = false;


    return {

        init: function() {
                  
    		$(document).ready(function() {  

                // ajout d'un select2
                if( $('select').length > 0 ) {
                    hasSelect = true;
                    
                    if (!App.Responsive.isXs()) {
                        $('select').each(function () {
                            $(this).select2();  
                        });
                        select2Activated = true;
                    }
                }
    		});


            $(window).on('resize', function() {
            
                // pas de select2 sur mobile
                if (App.Responsive.isXs()) {
                    // sm
                    if(hasSelect === true && select2Activated === true){
                        $('select').each(function () {
                            $(this).select2('destroy');  
                        });
                        select2Activated = false;
                    }
                } else {
                    if(hasSelect === true && select2Activated === false){
                        $('select').each(function () {
                            $(this).select2();  
                        });
                        select2Activated = true;
                    }
                }        
            });
        },

    };

})();
