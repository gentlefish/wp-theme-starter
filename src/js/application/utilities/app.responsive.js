App.Responsive = (function () {

    var $  = jQuery.noConflict();

    // VARIABLES

    // taille de l'element selon les media queries, telle que définies dans le css
    var XsCssHeight = 1,
        SCssHeight = 2,
        MCssHeight = 3,
        LCssHeight = 4,
        XlCssHeight = 5;

    var checkResponsiveClass = 'check-responsive' ;


    // ELEMENTS
    var $checkResponsive;


    return {

        init: function() {
            
            $(document).ready(function() {  

                // add element
                // l'idée est de checker la valeur de la propriété css "content" pour saveur en quelle largeur on est
                $('body').append('<div class="' + checkResponsiveClass + '"></div>')
                         .promise().done( function() {
                            $checkResponsive = $('.' + checkResponsiveClass);
                            // console.log(App.Responsive.isXs());
                         });
            });
        },


        // les propositions suivantes retournent true ou false

        isXs: function () {
            if ($checkResponsive.height() == XsCssHeight ) {
                return true;            
            } else {
                return false;
            }
        }, 

        

        isS: function () {
            if ($checkResponsive.height() == SCssHeight ) {
                return true;            
            } else {
                return false;
            }
        },

        isSAndMore: function () {
            if ($checkResponsive.height() >= SCssHeight ) {
                return true;            
            } else {
                return false;
            }
        }, 

        isSAndLess: function () {
            if ($checkResponsive.height() <= SCssHeight ) {
                return true;            
            } else {
                return false;
            }
        }, 



        isM: function () {
            if ($checkResponsive.height() == MCssHeight ) {
                return true;            
            } else {
                return false;
            }
        },

        isMAndMore: function () {
            if ($checkResponsive.height() >= MCssHeight ) {
                return true;            
            } else {
                return false;
            }
        }, 

        isMAndLess: function () {
            if ($checkResponsive.height() <= MCssHeight ) {
                return true;            
            } else {
                return false;
            }
        }, 



        isL: function () {
            if ($checkResponsive.height() == LCssHeight ) {
                return true;            
            } else {
                return false;
            }
        },

        isLAndMore: function () {
            if ($checkResponsive.height() >= LCssHeight ) {
                return true;            
            } else {
                return false;
            }
        }, 

        isLAndLess: function () {
            if ($checkResponsive.height() <= LCssHeight ) {
                return true;            
            } else {
                return false;
            }
        }, 



        isXl: function () {
            if ($checkResponsive.height() == XlCssHeight ) {
                return true;            
            } else {
                return false;
            }
        }
        
    };

})();
