
App.ScrollTo = (function () {

    var $  = jQuery.noConflict();


    // VARIABLES
    var scrollToClass = 'scroll-to';
    var easingDefault = 'easeOutQuint';
    var speedDefault = 500;
    var offsetHeight = 0;

    var scrollTopClass = 'scroll-top';
    var scrollTopHiddenClass = 'scroll-top--hidden';

    var speed,
        easing;

    // pour l'offset, on tient compte la hauteur du header / navbar
    var navbarClass = 'navbar--main';

    // ELEMENTS
    var $scrollTopElmt;


    return {

        init: function() {
            // console.log("coucou");

            $(document).ready(function() {

                // offsetHeight = $('.' + navbarClass).outerHeight() + 30;
                // offsetHeight = $('.' + navbarClass).outerHeight() - 3;
                offsetHeight = 0;

                $('.' + scrollToClass).on('click', function (event) {
                    event.preventDefault();
                    App.ScrollTo.scrollToElmt( $($(this).attr('href')), null, null );
                });


                // ajout du scroll top partout
                $('body').append('<a href="#" class="' + scrollTopClass + ' ' + scrollTopHiddenClass + '" aria-hidden="true"><i class="fa fa-angle-up" aria-hidden="true"></i></a>')
                        .promise().done(function () {

                            $scrollTopElmt = $('.' + scrollTopClass);

                            $scrollTopElmt.on('click', function (event) {
                                event.preventDefault();
                                App.ScrollTo.scrollToElmt( $('html'), null, null );
                            });


                            if( $scrollTopElmt.length > 0) {

                                App.ScrollTo.scrollTopToggleVisibility($scrollTopElmt);

                                $(window).on('scroll', function() {

                                    App.ScrollTo.scrollTopToggleVisibility($scrollTopElmt);

                                });

                            }
                        });
            });
        },


        scrollToElmt: function (elmt, speed, easing) {

            speed = speed || speedDefault;
            easing = easing || easingDefault;

            $('html, body').stop().animate({
                scrollTop: ($(elmt).position().top - offsetHeight) + 'px'
            }, speed, easing);
        },


        scrollTopToggleVisibility: function ($scrollTopElmt) {

            if ( $(window).scrollTop() > 0 ) {
                $scrollTopElmt.removeClass(scrollTopHiddenClass);
            } else {
                $scrollTopElmt.addClass(scrollTopHiddenClass);
            }
        }

    };

})();
