<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "main-content" and all content after.
 *
 * @package WordPress
 * @subpackage paolahivelin
 * @since paolahivelin 1.0
 */
?>
        </main>


        <footer class="main-footer" role="contentinfo">

            <div class="container main-footer__content">
                <section class="main-footer__item main-footer__item--center">

                </section>
            </div>
        </footer>



        <script>
            document.documentElement.className = document.documentElement.className.replace("no-js","js");
        </script>

        <?php wp_footer(); ?>

    </body>
</html>
