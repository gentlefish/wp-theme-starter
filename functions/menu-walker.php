<?php


class aria_walker extends Walker_Nav_Menu {
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="'. esc_attr( $class_names ) . '"';

        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .' role="menuitem">';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        $prepend = ''; // avant le texte du lien
        $append = ''; // après le texte du lien
        $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

        if($depth != 0) {
            $description = $append = $prepend = "";
        }

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
        $item_output .= $description.$args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}



/**
 * Add HTML suffix to nav active items in the main nav
 * => menu accessible
 */
class a42_active_item_walker extends Walker_Nav_Menu
{
    // Function responsible for the opening <li> tag and the link
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        $is_current_item = '';
        $is_current_item_class = '';
        // Replace 'twentynineteen' by your own text domain
        global $text_domain; if (!$text_domain && !isset($text_domain)) $text_domain = 'twentynineteen';
        // Get item classes
        $total = count($item->classes);
        $i = 0;
        // Loop through each class to display them accordingly
        foreach ($item->classes as $item_class){
            $is_current_item_class .= $item_class;
            // Insert white spaces if needed
            if ($i > 0 && $i != $total - 1) :
                $is_current_item_class .= ' ';
            endif;
            $i++;
        }
        // Add HTML in the active item
        if(array_search('current-menu-item', $item->classes) != 0)
        {
            $is_current_item = '<span class="sr-only">' . __(' – active page', $text_domain) . '</span>';
        }
        // Output result
        $output .= '<li class="'.$is_current_item_class.'"><a href="'.$item->url.'">'.$item->title . $is_current_item;
    }
    // Function responsible for the closing </li> tag is end_el
    function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= '</a></li>';
    }
}