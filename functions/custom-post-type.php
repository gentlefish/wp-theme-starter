<?php

function wpc_cpt_serie() {

    /* Property */
    $labels = array(
        'name'                => _x('Series', 'Post Type General Name', 'paolahivelin'),
        'singular_name'       => _x('Serie', 'Post Type Singular Name', 'paolahivelin'),
        'menu_name'           => __('Series d’oeuvres', 'paolahivelin'),
        'name_admin_bar'      => __('Series d’oeuvres', 'paolahivelin'),
        'parent_item_colon'   => __('Serie parente:', 'paolahivelin'),
        'all_items'           => __('Toutes les series', 'paolahivelin'),
        'add_new_item'        => __('Ajouter une nouvelle serie', 'paolahivelin'),
        'add_new'             => __('Ajouter', 'paolahivelin'),
        'new_item'            => __('Nouvelle serie', 'paolahivelin' ),
        'edit_item'           => __('Modifier une série', 'paolahivelin'),
        'update_item'         => __('Mettre à jour', 'paolahivelin'),
        'view_item'           => __('Voir la serie', 'paolahivelin'),
        'search_items'        => __('Rechercher un serie', 'paolahivelin'),
        'not_found'           => __('Not found', 'paolahivelin'),
        'not_found_in_trash'  => __('Not found in Trash', 'paolahivelin'),
    );
    $rewrite = array(
        'slug'                => _x('serie', 'serie', 'paolahivelin'),
        'with_front'          => true,
        'pages'               => true,
        'feeds'               => false,
    );
    $args = array(
        'label'               => __('serie', 'paolahivelin'),
        'description'         => __('Series', 'paolahivelin'),
        'labels'              => $labels,
        'supports'            => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'custom-fields'),
        'taxonomies'          => array('category'),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-format-gallery',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'query_var'           => 'serie',
        'rewrite'             => $rewrite,
        'capability_type'     => 'page',
    );
    register_post_type('serie', $args);
}

add_action('init', 'wpc_cpt_serie', 10);