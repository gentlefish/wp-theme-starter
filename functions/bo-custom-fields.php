<?php

// add custom fields in back office
if (!function_exists('faclab2016_add_custom_field_backoffice')) {
    function faclab2016_add_custom_field_backoffice ($post_id) {
        // die("test");
        if (isset($_GET['post'])) {
            $event_id = $_GET['post'];
            if (get_post_type($event_id)  == 'tribe_events' ) {
                global $wpdb;
                global $faclab2016_table_events_registrations;
                ?>
                    <div id="inscritsdiv" class="postbox ">
                        <button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">Ouvrir/fermer le bloc</span><span class="toggle-indicator" aria-hidden="true"></span></button>
                    <!-- <div id="normal-sortables" class="meta-box-sortables ui-sortable"> -->
                        <h2 class="hndle ui-sortable-handle"><span>Inscrits</span></h2>
                        <div class="inside">
                            <a href="data:text/csv;base64,<?php echo faclab2016_export_inscrits ($event_id); ?>" download="inscrits.csv" style="padding: 20px; display:block;">Télécharger la liste des inscrits</a>
                            <table class="wp-list-table widefat fixed striped" style="position: relative; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Utilisteur</th>
                                        <th>Inscrit ?</th>
                                        <th>Création</th>
                                        <th>Modification</th>
                                        <th>URL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $registeredUsers = $wpdb->get_results("SELECT * FROM $faclab2016_table_events_registrations WHERE idevent = '$event_id' ORDER BY inscription DESC" );

                                    if( count($registeredUsers) == 0) : ?>
                                        <tr>
                                            <td colspan="5">Aucun inscrit pour le moment</td>
                                        </tr>
                                    <?php else : ?>
                                        <?php foreach($registeredUsers as $registeredUser) : ?>
                                            <tr>
                                                <td>
                                                  <?php echo $registeredUser->email;  ?><br>
                                                  <?php echo $registeredUser->username;  ?>
                                                </td>
                                                <td><?php
                                                    switch ($registeredUser->inscription) {
                                                        case '0' :
                                                            echo 'désinscrit';
                                                            break;
                                                        case '1' :
                                                            echo 'attente';
                                                            break;
                                                        case '2' :
                                                            echo 'non confirmé';
                                                            break;
                                                        case '3' :
                                                            echo 'confirmé';
                                                            break;
                                                    }
                                                ?></td>
                                                <td><?php echo $registeredUser->timecreate;  ?></td>
                                                <td><?php echo $registeredUser->timeupdate;  ?></td>
                                                <td><?php
                                                        switch ($registeredUser->inscription) {
                                                            case '1' :
                                                            case '2' :
                                                                $user_url = faclab2016_urlValidation ($event_id, $registeredUser->email);
                                                                $user_url_text = 'confirmer';
                                                                echo '<a href="'.$user_url.'&freepass=1" target="_blank">'.$user_url_text.'</a>';
                                                                break;
                                                            case '3' :
                                                                $user_url = faclab2016_urlCancelation ($event_id, $registeredUser->email);
                                                                $user_url_text = 'déscinscrire';
                                                                echo '<a href="'.$user_url.'" target="_blank">'.$user_url_text.'</a>';
                                                                break;
                                                            default:
                                                                echo "-";
                                                        }
                                                    ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php
            }
        }
        return true;
    }
    add_action('edit_form_advanced', 'faclab2016_add_custom_field_backoffice');
}
