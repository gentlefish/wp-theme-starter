<?php

// Custom elements in TinyMCE
function wpb_mce_buttons_2($buttons) {
    // var_dump($buttons);
    // die();
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

if ( ! function_exists( 'tinyMCE_custom' ) ) :

    function tinyMCE_custom($init_array){

        $style_formats = array(
          // ajoute un type de mise en forme
          // array(
          //   'title' => 'Emphase',
          //   'block' => 'span',
          //   'classes' => 'emphasis',
          //   'wrapper' => false,
          // ),
          // array(
          //   'title' => 'Légende',
          //   'block' => 'span',
          //   'classes' => 'fl-caption',
          //   'wrapper' => false,
          // ),
          // array(
          //   'title' => 'Introduction',
          //   'block' => 'p',
          //   'classes' => 'fl-intro',
          //   'wrapper' => false,
          // ),
          // array(
          //   'title' => 'Bouttons',
          //   'block' => 'p',
          //   'classes' => 'buttons',
          //   'wrapper' => false,
          // ),
        );
        $init_array['style_formats'] = json_encode( $style_formats );

        $init_array['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;';
        // $init_array['theme_advanced_styles'] = 'Rouge=.empasis;';
        return $init_array;
    }
    add_filter('tiny_mce_before_init', 'tinyMCE_custom');
endif; // tinyMCE_custom
