<?php


function theme_load_scripts() {

    // ajoute le script
    // wp_enqueue_script( 'gow_vendors', get_template_directory_uri().'/assets/js/vendors/vendors.js', array('jquery'), false, true );
    wp_enqueue_script( 'gow_application', get_template_directory_uri().'/assets/js/application.js', array('jquery'), false, true );
    // wp_enqueue_script( 'gow_application', get_template_directory_uri().'/test-ajaxify.js', array('jquery'), false, true );
}
add_action('wp_enqueue_scripts', 'theme_load_scripts');

