<?php

if ( ! function_exists( 'theme_textdomain' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since theme 1.0
 */
function theme_textdomain() {

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on twentyfifteen, use a find and replace
     * to change 'twentyfifteen' to the name of your theme in all the template files
     */
    load_theme_textdomain( 'paolahivelin', get_template_directory() . '/languages' );

}
endif; // gow_setup
add_action( 'after_setup_theme', 'theme_textdomain' );

