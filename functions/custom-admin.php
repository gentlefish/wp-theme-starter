<?php


// supprime les commentaires du menu d'admin
if ( ! function_exists( 'theme_custom_menu_page_removing' ) ) :

    function theme_custom_menu_page_removing() {

        remove_menu_page( 'edit-comments.php' );
        // remove_menu_page( 'edit.php' );

        // events
        // remove_submenu_page('edit.php?post_type=tribe_events', 'edit.php?post_type=tribe_venue');
        // remove_submenu_page('edit.php?post_type=tribe_events', 'edit.php?post_type=tribe_organizer');

        // ne fonctionnenent pas car pas ajouter avce add_submenu_page
        // remove_submenu_page('edit-tags.php?taxonomy=post_tag&post_type=tribe_events', '');
        // remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag&post_type=aggregator');
        // remove_submenu_page('edit.php?post_type=tribe_events', 'edit.php?post_type=tribe_events&page=tribe-app-shop');
        // remove_submenu_page('edit.php?post_type=tribe_events', 'edit.php?post_type=tribe_events&page=tribe-common');
        // remove_submenu_page( 'edit.php?post_type=tribe_events', 'tribe-app-shop' );
    }

    add_action( 'admin_menu', 'theme_custom_menu_page_removing' );
endif; // custom_menu_page_removing




// Remove tags from back office
// http://wordpress.stackexchange.com/questions/110782/remove-categories-tags-from-admin-menu
if (!function_exists('theme_remove_sub_menus')) {

  function theme_remove_sub_menus() {

      remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');

  }
  add_action('admin_menu', 'theme_remove_sub_menus');


  function theme_remove_post_metaboxes() {

    remove_meta_box( 'tagsdiv-post_tag','post','normal' ); // Tags Metabox

  }
  add_action('admin_menu','theme_remove_post_metaboxes');
}