<?php

if ( ! function_exists( 'theme_imgesizes' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since gow 1.0
 */
function theme_imgesizes() {

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
     */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 280, 190, true );

    remove_image_size( 'post-thumbnail' );
    remove_image_size( 'medium' );
    remove_image_size( 'medium_large' );
    // remove_image_size( 'large' );

    // image inérée dans les contenus
    // update_option( 'large_size_w', 800 );
    // update_option( 'large_size_h', 9999 );
    // update_option( 'large_crop', false );
    // update_option( 'image_default_size', 'large' );


    // Add image formats, format 16/9
    // add_image_size( 'gow_cover', 1500, 800, true );
    // add_image_size( 'gow_cover@2x', 3000, 1600, true );
    // add_image_size( 'gow_cover_mini', 800, 800, true );

    // add_image_size( 'gow_coverheader', 1500, 300, true );
    // add_image_size( 'gow_coverheader@2x', 3000, 600, true );
    // add_image_size( 'gow_coverheader_mini', 800, 400, true );

    // add_image_size( 'gow_thumbnail', 360, 220, true );
    // add_image_size( 'gow_thumbnail@2x', 720, 440, true );

    // add_image_size( 'gow_slideshow', 440, 350, true );
    // add_image_size( 'gow_slideshow@2x', 880, 700, true );
    // // add_image_size( 'gow_slideshow_full', 1200, 700, true ); // on affiche l'originale

    // add_image_size( 'gow_square', 320, 320, true );
    // add_image_size( 'gow_square@2x', 640, 640, true );


    // reco image de partage : https://blog.bufferapp.com/ideal-image-sizes-social-media-posts
    // add_image_size( 'share', 1200, 628, true );

}
endif; // gow_setup
add_action( 'after_setup_theme', 'theme_imgesizes' );





function gow_remove_default_image_sizes( $sizes) {

    // not working
    // unset( $sizes['thumbnail']);
    // unset( $sizes['medium']);
    // unset( $sizes['medium_large']);
    // unset( $sizes['large']);

    // working
    // unset( $sizes[0]);  // thumbnail
    // unset( $sizes[1]);  // medium
    // unset( $sizes[2]);  // medium_large
    // unset( $sizes[3]);  // large

    return $sizes;
}
add_filter('intermediate_image_sizes', 'gow_remove_default_image_sizes');

