<?php

// CUSTOM GALLERY

add_filter( 'use_default_gallery_style', '__return_false' );
add_filter('post_gallery','customFormatGallery',10,2);

function customFormatGallery($string,$attr){

    $output = '<div class="swiper-container">';
    $output .= '<ul class="swiper-wrapper gallery">';
    $posts = get_posts(array('include' => $attr['ids'],'post_type' => 'attachment'));

    foreach($posts as $imagePost){

       $output .= '<li class="swiper-slide">
          <img src="'. wp_get_attachment_image_src($imagePost->ID, 'gow_slideshow@2x')[0].'" alt="" />
          <a class="gallery-zoom" href="'. wp_get_attachment_image_src($imagePost->ID, 'gow_slideshow@2x')[0] .'"></a>
        </li>';
    }

    $output .= "</ul>";
   $output .=  '<div class="swiper-buttons">
        <div class="swiper-button-prev"></div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next"></div>
    </div>';
    $output .= "</div>";

    return $output;
}



// add_filter( 'use_default_gallery_style', '__return_false' );
// // modify attr
// remove_shortcode('gallery');
// add_shortcode('gallery', 'my_own_gallery_shortcode');
// function my_own_gallery_shortcode($attr) {
//     $attr['itemtag']      = 'div';
//     $attr['icontag']      = 'span';
//     $attr['captiontag']   = 'figcaption';
//     $attr['size']         = 'square';
//     $output = gallery_shortcode($attr) ;
//     $output = preg_replace(array('/<br[^>]*>/', '/<span[^>]*>/', '/<\/span>/'), '', $output);
//     return $output;
// }