<?php

// Replace Posts label as Articles in Admin Panel
if (!function_exists('faclab2016_change_post_menu_label')) {

  function faclab2016_change_post_menu_label() {
      global $menu;
      global $submenu;
      $menu[5][0] = 'Articles';
      $submenu['edit.php'][5][0] = 'Articles';
      // $submenu['edit.php'][10][0] = 'Ajouter un billet';
      echo '';
  }
  add_action( 'admin_menu', 'faclab2016_change_post_menu_label' );

  function faclab2016_change_post_object_label() {
          global $wp_post_types;
          $labels = &$wp_post_types['post']->labels;
          $labels->name = 'Articles';
          $labels->singular_name = 'Article';
          $labels->new_item = 'Article';
          $labels->view_item = 'Voir l\'article';
          $labels->search_items = 'Rechercher dans les articles';
          $labels->not_found = 'Aucun article trouvé';
          $labels->not_found_in_trash = 'Aucun article trouvé dans la corbeille';
  }
  add_action( 'init', 'faclab2016_change_post_object_label' );
}
