<?php




/**
 * Retrieve the file type based on the extension name.
 *
 * @since 2.5.0
 *
 * @param string $ext The extension to search.
 * @return string|void The file type, example: audio, video, document, spreadsheet, etc.
 */
if ( ! function_exists( 'faclab2016_wp_ext2type' ) ) :
function freakmagnet_wp_ext2type( $ext ) {
  $ext = strtolower( $ext );

  /**
   * Filter file type based on the extension name.
   *
   * @since 2.5.0
   *
   * @see wp_ext2type()
   *
   * @param array $ext2type Multi-dimensional array with extensions for a default set
   *                        of file types.
   */
  $ext2type = apply_filters( 'ext2type', array(
    'image'       => array( 'jpg', 'jpeg', 'jpe',  'gif',  'png',  'bmp',   'tif',  'tiff', 'ico' ),
    'audio'       => array( 'aac', 'ac3',  'aif',  'aiff', 'm3a',  'm4a',   'm4b',  'mka',  'mp1',  'mp2',  'mp3', 'ogg', 'oga', 'ram', 'wav', 'wma' ),
    'video'       => array( '3g2',  '3gp', '3gpp', 'asf', 'avi',  'divx', 'dv',   'flv',  'm4v',   'mkv',  'mov',  'mp4',  'mpeg', 'mpg', 'mpv', 'ogm', 'ogv', 'qt',  'rm', 'vob', 'wmv' ),
    'document'    => array( 'doc', 'docx', 'docm', 'dotm', 'odt',  'pages',  'xps',  'oxps', 'rtf',  'wp', 'wpd', 'psd', 'xcf' ),
    'spreadsheet' => array( 'numbers',     'ods',  'xls',  'xlsx', 'xlsm',  'xlsb' ),
    'interactive' => array( 'swf', 'key',  'ppt',  'pptx', 'pptm', 'pps',   'ppsx', 'ppsm', 'sldx', 'sldm', 'odp' ),
    'text'        => array( 'asc', 'csv',  'tsv',  'txt' ),
    'archive'     => array( 'bz2', 'cab',  'dmg',  'gz',   'rar',  'sea',   'sit',  'sqx',  'tar',  'tgz',  'zip', '7z' ),
    'code'        => array( 'css', 'htm',  'html', 'php',  'js' ),
    'pdf'         => array( 'pdf' ),
  ) );

  foreach ( $ext2type as $type => $exts )
    if ( in_array( $ext, $exts ) )
      return $type;
}
endif;

// Récupère le poids du fichier
if ( ! function_exists( 'getSize' ) ) :
function getSize ( $file ) {
  $bytes = filesize($file);
  $s = array('b', 'Kb', 'Mb', 'Gb');
  $e = floor(log($bytes)/log(1024));
  return sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
}
endif;
