<?php

if ( ! function_exists( 'theme_menu' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since gow 1.0
 */
function theme_menu() {

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'main_menu' => 'Menu principal',
        // 'icones_header_menu' => 'Menu icones haut',
        // 'icones_footer_menu' => 'Menu icones bas',
    ) );
}
endif; // gow_setup
add_action( 'after_setup_theme', 'theme_menu' );






