<?php

// Add CSS stylesheet for admin
if ( ! function_exists( 'admin_css' ) ) :

  function admin_css() {

    $admin_handle = 'admin_css';
    $admin_stylesheet = get_template_directory_uri() . '/assets/css/admin.css';

    wp_enqueue_style( $admin_handle, $admin_stylesheet );
  }
  add_action('admin_print_styles', 'admin_css', 11 );
endif; // admin_css
