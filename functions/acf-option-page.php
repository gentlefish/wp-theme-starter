<?php

// ajoute les options du thème
if ( function_exists('acf_add_options_page') ) {
    // acf_add_options_page(
    //   array(
    //     'page_title'    => 'Infos',
    //     'menu_title'    => 'Infos',
    //     'menu_slug'     => 'infos-gow',
    //     'capability'    => 'edit_posts',
    //     'redirect'      => false
    //   ));
    acf_add_options_page(
      array(
        'page_title'    => 'Footer',
        'menu_title'    => 'Footer',
        'menu_slug'     => 'footer-gow',
        'capability'    => 'edit_posts',
        'redirect'      => false
      ));
}
