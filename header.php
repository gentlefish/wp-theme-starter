<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "main-content"
 *
 * @package WordPress
 * @subpackage paolahivelin
 * @since paolahivelin 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <!-- <meta name="description" content="<?php bloginfo( 'name' ); ?>, <?php bloginfo( 'description' ); ?>"> -->

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="msapplication-TileColor" content="#000">
        <meta name="theme-color" content="#000">

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css">

        <!--[if lte IE 9]>
            <script src="<?php echo get_template_directory_uri(); ?>/assets/js/ie/placeholders.min.js"></script>
        <![endif]-->

        <!--[if lte IE 8]>
            <script src="<?php echo get_template_directory_uri(); ?>/assets/js/ie/html5.js"></script>
            <script src="<?php echo get_template_directory_uri(); ?>/assets/js/ie/respond.min.js"></script>
        <![endif]-->

        <?php wp_head(); ?>

        <title><?php bloginfo('name'); ?> / <?php wp_title(); ?></title>

    </head>

    <body <?php body_class('body'); ?>>
        <!--[if lt IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <a href="#main-nav" class="sr-only sr-only-focusable"><?php __('Aller au menu'); ?></a>
        <a href="#main-content" class="sr-only sr-only-focusable"><?php __('Aller au contenu'); ?></a>

        <header class="main-header" role="banner">
            <div class="main-header__content">
                <div class="container-page">
                    <h1 class="main-header__title">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>">
                            <i class="c-icon">
                                <?php // echo file_get_contents( get_stylesheet_directory_uri() . '/assets/img/identity/title.svg' ); ?>
                            </i>
                            <span class="visuallyhidden" lang="en">Gang of Witches</span>
                        </a>
                    </h1>


                    <div class="main-header__logo">
                        <i class="c-icon" >
                            <?php // echo file_get_contents( get_stylesheet_directory_uri() . '/assets/img/identity/logo.svg' ); ?>
                        </i>
                        <div class="main-nav__toggle__wrapper">
                            <button class="main-nav__toggle burger">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span class="visuallyhidden"><?php __('Ouvrir le menu', 'gow'); ?></span>
                            </button>
                        </div>
                    </div>


                </div>
            </div>

            <div class="main-header__nav" id="main-nav" role="navigation" tabindex="-1">
                <div class="main-nav">
                    <div class="main-nav__container container-page">
                        <nav class="nav" role="navigation">
                            <?php
                            // $main_menu_args = array(
                            //     'theme_location' => 'main_menu',
                            //     'container' => false,
                            //     'items_wrap' => '<ul id="%1$s" class="%2$s" role="menu">%3$s</ul>',
                            //     );
                            // wp_nav_menu($main_menu_args);
                            ?>
                        </nav>
                    </div>

                </div>
            </div>

        </header>



        <main id="main-content" class="content-main" role="main" tabindex="-1">
