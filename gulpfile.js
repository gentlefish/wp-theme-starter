/*
 * Gulpfile
 */

// Load plugins
const gulp         = require('gulp');
const sass         = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS     = require('gulp-clean-css');
const uglify       = require('gulp-uglify');
const imagemin     = require('gulp-imagemin');
const rename       = require('gulp-rename');
const browserSync  = require('browser-sync');
const sourcemaps   = require("gulp-sourcemaps");
const concat       = require("gulp-concat");

// Variables
var reload = browserSync.reload;

// Paths
var srcPath = 'src';
var distPath = 'assets';
var paths = {
  styles: srcPath + '/scss',
  scripts: srcPath + '/js',
  images: srcPath + '/img',
  fonts: srcPath + '/fonts',
  vendors: 'node_modules',
};


// Styles
gulp.task('styles', function() {
  return gulp.src(paths.styles + '/*.scss')
    .pipe(sass())
    .on('error', swallowError)
    .pipe(autoprefixer())
    // .pipe(cleanCSS())
    .on('error', swallowError)
    // .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(distPath + '/css'))
    .pipe(reload({stream: true}));
});

gulp.task('styles-editor', function() {
  return gulp.src(paths.styles + '/editor-style.scss')
    .pipe(sass())
    .on('error', swallowError)
    .pipe(autoprefixer())
    // .pipe(cleanCSS())
    .on('error', swallowError)
    // .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('.'))
    .pipe(reload({stream: true}));
});

// Scripts : copy
gulp.task('scripts-ie', function() {
  // console.log('scripts ie');
  return gulp.src(paths.scripts + '/ie/**/*.js')
    .pipe(uglify())
    .on('error', swallowError)
    // .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(distPath + '/js/ie'));
    // .on('end', reload);
});


// scripts : concat
gulp.task('scripts-app', function () {
    // console.log(paths.scripts + '/application/wrapper/app.js-header.js');
    // return gulp.src(["lib/*.{js,json}", "lib/**/*.{js,json}"])
    return gulp.src([

              // paths.scripts + '/application/vendors/scrollify/jquery.scrollify.js',
              // paths.vendors + '/jquery.easing/jquery.easing.js',
              // paths.vendors + '/imagesloaded/imagesloaded.pkgd.js',
              // paths.vendors + '/svg4everybody/dist/svg4everybody.js',
              // paths.vendors + '/sticky-sidebar/dist/jquery.sticky-sidebar.js',
              // paths.vendors + '/swiper/dist/js/swiper.js',
              // paths.vendors + '/simplelightbox/dist/simple-lightbox.js',
              paths.scripts + '/application/wrapper/app.js-header.js',
              paths.scripts + '/application/wrapper/app.js-init.js',
              paths.scripts + '/application/content/app.layout.js',
              // paths.scripts + '/application/utilities/app.scroll-to.js',
              // paths.scripts + '/application/components/app.scrolling.js',
              // paths.scripts + '/application/components/app.letter-defil.js',
              // paths.scripts + '/application/components/app.popin.js',
              paths.scripts + '/application/wrapper/app.js-footer.js'
              ])
          .pipe(sourcemaps.init())
          .pipe(concat('application.js'))
          .pipe(sourcemaps.write())
          .pipe(gulp.dest(distPath + '/js'))
          .on('end', reload);
});

// Images
gulp.task('images', function() {
  return gulp.src(paths.images + '/**/*.{gif,jpg,png}')
    .pipe(imagemin({
            progressive: true
        }))
    .on('error', swallowError)
    .pipe(gulp.dest(distPath + '/img'))
    .on('end', reload);
});

// SVG
gulp.task('svg', function () {
  return gulp.src(paths.images + '/**/*.svg')
    .pipe(gulp.dest(distPath + '/img'))
    .on('end', reload);
});

// Fonts
gulp.task('fonts', function() {
  return gulp.src(paths.fonts + '/**/*')
    .pipe(gulp.dest(distPath + '/fonts'))
    .on('end', reload);
});
gulp.task('font-awesome', function() {
  return gulp.src(paths.vendors + '/font-awesome/fonts/*')
    .pipe(gulp.dest(distPath + '/fonts'))
    .on('end', reload);
});


// Watch
// gulp.task('templates-watch', ['templates'], reload);

// Prevent errors from breaking gulp watch
function swallowError (error) {
  console.log(error.toString());
  this.emit('end');
}

gulp.task('init', gulp.series(['font-awesome', 'scripts-ie']));
gulp.task('all', gulp.series(['styles', 'styles-editor', 'scripts-app', 'images', 'svg', 'fonts', 'font-awesome', 'scripts-ie']));

// Build: one shot
gulp.task('build', gulp.series(['styles', 'styles-editor', 'scripts-app', 'images', 'svg', 'fonts']));

// Default: watch changes
gulp.task('watch', gulp.series(['build'], function() {
  browserSync.init({
    proxy: "localhost:8888/paolahivelin/",
    notify: false
  });

  gulp.watch(paths.styles + '/**/*.scss',  gulp.series(['styles']));
  gulp.watch(paths.scripts + '/**/*.js',  gulp.series(['scripts-app']));
  gulp.watch(paths.images + '/**/*',  gulp.series(['images', 'svg']));
  gulp.watch(paths.fonts + '/**/*',  gulp.series(['fonts']));
}));
