<?php
/**
 *
 * @package WordPress
 * @subpackage lafleche
 * @since lafleche 1.0
 */

// ceci est le template par défaut

get_header(); ?>

<?php
// Start the loop.
while ( have_posts() ) : the_post();

?>
<article class="page template--default page--404">

    <div class="page__header page-header">
        <div class="container-page">
            <h1 class="page-header__title">
                <small>Il y a eu un problème</small>
                Erreur 404
            </h1>
            <a href="<?php echo home_url(); ?>">Cliquez ici pour revenir à la home.</a>
        </div>
    </div>

</article>
<?php
// End the loop.
endwhile;
?>

<?php get_footer(); ?>
