<?php

// fonctions divers


function fleche_day_numeric_to_french ($timetamp) {

    $day_week = array(
        1 => 'lundi',
        2 => 'mardi',
        3 => 'mercredi',
        4 => 'jeudi',
        5 => 'vendredi',
        6 => 'samedi',
        7 => 'dimanche'
    );

    return $day_week [ date( 'N', $timetamp ) ];
}

function fleche_month_numeric_to_french ($timetamp) {

    $day_week = array(
        1 => 'janvier',
        2 => 'février',
        3 => 'mars',
        4 => 'avril',
        5 => 'mai',
        6 => 'juin',
        7 => 'juillet',
        8 => 'août',
        9 => 'septembre',
        10 => 'octobre',
        11 => 'novembre',
        12 => 'décembre'
    );

    return $day_week [ date( 'n', $timetamp ) ];
}


// timestamps en paramètres
function faclab2016_event_duration ($ts_date_start, $ts_date_end) {

    $ts_duration = $ts_date_end - $ts_date_start;

    $hours = floor($ts_duration / 60 / 60);
    $minutes = ($ts_duration % 60);

    if( $minutes  < 10 ) {
        $minutes = '0' . $minutes;
    }

    return ($hours . 'h' . $minutes) ; // minutes
}




if ( class_exists('Tribe__Events__Main') ){

    /* get event category names in text format */
    function tribe_get_text_categories ( $event_id = null ) {

        if ( is_null( $event_id ) ) {
            $event_id = get_the_ID();
        }

        $event_cats = '';

        $term_list = wp_get_post_terms( $event_id, Tribe__Events__Main::TAXONOMY );

        foreach( $term_list as $term_single ) {
            $event_cats .= $term_single->name . ', ';
        }

        return rtrim($event_cats, ', ');

    }


    function tribe_get_event_categories_list( $post_id = null, $args = array() ) {
        $events_label_singular = tribe_get_event_label_singular();

        $post_id    = is_null( $post_id ) ? get_the_ID() : $post_id;
        $defaults   = array(
            'echo'         => false,
            'wrap_before'  => '<ul class="tribe-event-categories">',
            'wrap_after'   => '</ul>',
        );
        $args       = wp_parse_args( $args, $defaults );
        $categories = tribe_get_event_taxonomy( $post_id, $args );


        $html = ! empty( $categories ) ? sprintf(
            '%s%s%s',
            $args['wrap_before'],
            $categories,
            $args['wrap_after']
        ) : '';
        if ( $args['echo'] ) {
            echo apply_filters( 'tribe_get_event_categories', $html, $post_id, $args, $categories );
        } else {
            return apply_filters( 'tribe_get_event_categories', $html, $post_id, $args, $categories );
        }
    }

}

