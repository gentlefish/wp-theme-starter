<?php

function picture ($image, $image_retina, $image_small, $image_alt = '', $image_class = '', $caption = '') {

    if(!empty($image)) {

        $picture = '';

        if($caption != '') {
            $picture .= '<figure>';
        }

        $picture .= '<picture class="' . $image_class . '">';
        $picture .= '<!--[if IE 9]><video style="display: none;"><![endif]-->';
        $picture .= '<source srcset="' . $image_small . '" media="(max-width : 800px)">';
        $picture .= '<source srcset="' . $image . ', ' . $image_retina . ' 2x">';
        $picture .= '<!--[if IE 9]></video><![endif]-->';
        $picture .= '<img src="' . $image . '" alt="' . $image_alt . '" loading="lazy">';
        $picture .= '</picture>';

        if($caption != '') {
            $picture .= '<figcaption>' . $caption . '</figcaption>';
            $picture .= '</figure>';
        }


    } else {
        return;
    }

    return $picture;
}
