
(function($) {

    'use strict';

    var App = {};


function init() {

    App.Layout.init();
    // App.ScrollTo.init();
    // App.Scrolling.init();
    // App.Popin.init();

}


App.Layout = (function () {

    var $  = jQuery.noConflict();

    // VARIABLES

    // ELEMENTS

    return {

        init: function () {

            $(document).ready( function() {

                $('.main-nav__toggle').on('click', function (event) {
                    event.preventDefault();
                    $('body').toggleClass('body--menu-visible');
                });


                // $('.accordeon').on('click', function (event) {
                //     event.preventDefault();
                //     $(this).toggleClass('accordeon--open');
                // })


                // $('.main-nav .menu-item-has-children a').on('click', function (event) {
                //     console.log($('.main-nav .menu-item-has-children'));
                //     $('.main-nav .menu-item-has-children').removeClass('submenu-open');
                //     $(this).parent('.menu-item-has-children').toggleClass('submenu-open')
                // });

                //swiper
                // var swiperArticles = new Swiper('.swiper-container', {
                //     loop: true,
                //     navigation: {
                //         nextEl: '.swiper-button-next',
                //         prevEl: '.swiper-button-prev',
                //     },
                //     pagination: {
                //         el: '.swiper-pagination',
                //         type: 'fraction',
                //     },
                // });


                // // lightbox
                // var lightbox = $('.gallery a').simpleLightbox();


            });

        },

    }

})();


	init();

}(jQuery));


//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcy1oZWFkZXIuanMiLCJhcHAuanMtaW5pdC5qcyIsImFwcC5sYXlvdXQuanMiLCJhcHAuanMtZm9vdGVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ0xBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNWQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDMURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhcHBsaWNhdGlvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuKGZ1bmN0aW9uKCQpIHtcblxuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIHZhciBBcHAgPSB7fTsiLCJcblxuZnVuY3Rpb24gaW5pdCgpIHtcblxuICAgIEFwcC5MYXlvdXQuaW5pdCgpO1xuICAgIC8vIEFwcC5TY3JvbGxUby5pbml0KCk7XG4gICAgLy8gQXBwLlNjcm9sbGluZy5pbml0KCk7XG4gICAgLy8gQXBwLlBvcGluLmluaXQoKTtcblxufVxuIiwiXG5BcHAuTGF5b3V0ID0gKGZ1bmN0aW9uICgpIHtcblxuICAgIHZhciAkICA9IGpRdWVyeS5ub0NvbmZsaWN0KCk7XG5cbiAgICAvLyBWQVJJQUJMRVNcblxuICAgIC8vIEVMRU1FTlRTXG5cbiAgICByZXR1cm4ge1xuXG4gICAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcblxuICAgICAgICAgICAgJChkb2N1bWVudCkucmVhZHkoIGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgICAgICAgJCgnLm1haW4tbmF2X190b2dnbGUnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgJCgnYm9keScpLnRvZ2dsZUNsYXNzKCdib2R5LS1tZW51LXZpc2libGUnKTtcbiAgICAgICAgICAgICAgICB9KTtcblxuXG4gICAgICAgICAgICAgICAgLy8gJCgnLmFjY29yZGVvbicpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgIC8vICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIC8vICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdhY2NvcmRlb24tLW9wZW4nKTtcbiAgICAgICAgICAgICAgICAvLyB9KVxuXG5cbiAgICAgICAgICAgICAgICAvLyAkKCcubWFpbi1uYXYgLm1lbnUtaXRlbS1oYXMtY2hpbGRyZW4gYScpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgIC8vICAgICBjb25zb2xlLmxvZygkKCcubWFpbi1uYXYgLm1lbnUtaXRlbS1oYXMtY2hpbGRyZW4nKSk7XG4gICAgICAgICAgICAgICAgLy8gICAgICQoJy5tYWluLW5hdiAubWVudS1pdGVtLWhhcy1jaGlsZHJlbicpLnJlbW92ZUNsYXNzKCdzdWJtZW51LW9wZW4nKTtcbiAgICAgICAgICAgICAgICAvLyAgICAgJCh0aGlzKS5wYXJlbnQoJy5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJykudG9nZ2xlQ2xhc3MoJ3N1Ym1lbnUtb3BlbicpXG4gICAgICAgICAgICAgICAgLy8gfSk7XG5cbiAgICAgICAgICAgICAgICAvL3N3aXBlclxuICAgICAgICAgICAgICAgIC8vIHZhciBzd2lwZXJBcnRpY2xlcyA9IG5ldyBTd2lwZXIoJy5zd2lwZXItY29udGFpbmVyJywge1xuICAgICAgICAgICAgICAgIC8vICAgICBsb29wOiB0cnVlLFxuICAgICAgICAgICAgICAgIC8vICAgICBuYXZpZ2F0aW9uOiB7XG4gICAgICAgICAgICAgICAgLy8gICAgICAgICBuZXh0RWw6ICcuc3dpcGVyLWJ1dHRvbi1uZXh0JyxcbiAgICAgICAgICAgICAgICAvLyAgICAgICAgIHByZXZFbDogJy5zd2lwZXItYnV0dG9uLXByZXYnLFxuICAgICAgICAgICAgICAgIC8vICAgICB9LFxuICAgICAgICAgICAgICAgIC8vICAgICBwYWdpbmF0aW9uOiB7XG4gICAgICAgICAgICAgICAgLy8gICAgICAgICBlbDogJy5zd2lwZXItcGFnaW5hdGlvbicsXG4gICAgICAgICAgICAgICAgLy8gICAgICAgICB0eXBlOiAnZnJhY3Rpb24nLFxuICAgICAgICAgICAgICAgIC8vICAgICB9LFxuICAgICAgICAgICAgICAgIC8vIH0pO1xuXG5cbiAgICAgICAgICAgICAgICAvLyAvLyBsaWdodGJveFxuICAgICAgICAgICAgICAgIC8vIHZhciBsaWdodGJveCA9ICQoJy5nYWxsZXJ5IGEnKS5zaW1wbGVMaWdodGJveCgpO1xuXG5cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIH0sXG5cbiAgICB9XG5cbn0pKCk7XG4iLCJcblx0aW5pdCgpO1xuXG59KGpRdWVyeSkpO1xuXG4iXX0=
