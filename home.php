<?php
/**
 * Template Name: Home
 *
 * @package WordPress
 * @subpackage paolahivelin
 * @since paolahivelin 1.0
 */

// Template de la home

get_header(); ?>

<?php
// Start the loop.
while ( have_posts() ) : the_post();
    $post_id = get_the_ID();

?>


    <article class="container container-page home-container">

        <div class="home_content">
            <h1 class="home_content__title"><?php the_title(); ?></h1>
            <div class="home_content__text">
                <?php the_content(); ?>
            </div>
        </div>

    </article>


<?php
// End the loop.
endwhile;
?>


<?php get_footer(); ?>
