<?php
/**
 *
 * @package WordPress
 * @subpackage paolahivelin
 * @since paolahivelin 1.0
 */


include_once 'inc/utilities.php';
include_once 'inc/picture.php';


// //inlcude modules when needed

// // wp retro-compatibility
// include_once 'functions/retro-compatibility.php';



// // setup
include_once 'functions/setup.php';

// // textdomain
include_once 'functions/textdomain.php';

// // images sizes
include_once 'functions/images-sizes.php';

// // menus
include_once 'functions/menus.php';



// // // Edit menus in admin
// // // Remove unused pages
// // // Remove tags from back office
// // ERROR
include_once 'functions/custom-admin.php';

// // add CSS for admin
include_once 'functions/admin-css.php';


// // add custom post type(s)
include_once 'functions/custom-post-type.php';


// // change posts labels
// // include_once 'functions/change-post-labels.php';

// // add custom fields in back office
// // include_once 'functions/bo-custom-fields.php';


// // // ACF : add option page(s)
// include_once 'functions/acf-option-page.php';


// // TinyMCE : buttons, custom class to show in BO
include_once 'functions/custom-tiny-mce.php';



// add scripts
include_once 'functions/scripts.php';

// // custom html for gallery
include_once 'functions/custom-gallery.php';



// // Menu Walker
// // include_once 'functions/menu-walker.php';



// // File type
// include_once 'functions/file-type.php';




function my_rewrite_flush() {
    flush_rewrite_rules();
    // wpc_cpt_serie();
}
add_action( 'after_switch_theme', 'my_rewrite_flush' );

