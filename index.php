<?php
/**
 *
 * @package WordPress
 * @subpackage paolahivelin
 * @since paolahivelin 1.0
 */

// ceci est le template par défaut

get_header(); ?>

<?php
// Start the loop.
while ( have_posts() ) : the_post();

?>
<article class="page template--default template--simple">

    <header class="page__header page-header">
        <h1 class="page-header__title">
            <?php the_title(); ?>
        </h1>
    </header>

    <div class="container-page">
        <div class="editor-content page__content">
            <?php the_content();?>
        </div>
    </div>

</article>
<?php
// End the loop.
endwhile;
?>

<?php get_footer(); ?>
